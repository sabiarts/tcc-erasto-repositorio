﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject sickCell;
    public GameObject whiteCell;
    public GameObject[] otherCells;

    public float spawnMargin;
    public float spawnRatio;
    public float sickRatio;
    

    GameObject[] spawnItems = new GameObject[4];

    GameMaster gameMaster;
    Checkpoint firstCheckpoint;


    void Start()
    {
        spawnItems = new GameObject[] {sickCell, whiteCell, otherCells[0], otherCells[1], otherCells[1], otherCells[1]};
        gameMaster = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameMaster>();
        firstCheckpoint = GetComponent<Checkpoint>();
        StartCoroutine(SpawnCells());
        //Debug.Log(gameMaster);
    }

    IEnumerator SpawnCells(){
        Vector2 spawnPoint;
        int spawnId = 0;
        int sickOrNot = 0;
        while (true){
            yield return new WaitForSeconds(spawnRatio);
            sickOrNot = Random.Range(0, 100);
            if (sickOrNot < sickRatio){
                spawnId = 0;
            } else{
                spawnId = (int) Random.Range(1, spawnItems.Length);
            }
            spawnPoint = new Vector2(transform.position.x + Random.Range(spawnMargin * -1, spawnMargin), transform.position.y + Random.Range(spawnMargin * -1, spawnMargin));
            GameObject cell = Instantiate(spawnItems[spawnId], spawnPoint, transform.rotation);
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), cell.GetComponent<Collider2D>());
            cell.GetComponent<Cell>().SetDirection(firstCheckpoint.Redirect());
            gameMaster.allCells.Add(cell);
        }
    }
}
