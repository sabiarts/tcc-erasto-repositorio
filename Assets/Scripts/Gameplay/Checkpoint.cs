﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public int directionInDegrees = 0;

    /* Direções dos vetores:
    0º  :  1, 0
	45º :  0.7f, 0.7f
	90º :  0, 1
	135º: -0.7f, 0.7f
	180º: -1, 0
	225º: -0.7f, -0.7f
	270º:  0, -1
	315º:  0.7f, -0.7f */
    
    public Vector2 Redirect(){
        switch (directionInDegrees){
            case 0:
                return new Vector2 (1, 0);
            case 45:
                return new Vector2 (0.7f, 0.7f);
            case 90:
                return new Vector2 (0, 1);
            case 135:
                return new Vector2 (-0.7f, 0.7f);
            case 180:
                return new Vector2 (-1, 0);
            case 225:
                return new Vector2 (-0.7f, -0.7f);
            case 270:
                return new Vector2 (0, -1);
            case 315:
                return new Vector2 (0.7f, -0.7f);
            default:
                Debug.LogError("The value assigned to the variable 'directionInDegrees' is invalid");
                return new Vector2 (1, 0);
        }
    }
}
