using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.AnimationModule;

public class Cell : MonoBehaviour{
    Vector2 direction = new Vector2(1, 0);
    public bool sick;
    public bool white;

    bool hit = false;
    SpriteRenderer ownSprite;

    void Start(){
        ownSprite = GetComponent<SpriteRenderer>();
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(),GameObject.FindGameObjectWithTag("Finish").GetComponent<Collider2D>());
    }

    public Vector2 GetDirection(){
        return direction;
    }

    public void SetDirection(Vector2 newDirection){
        direction = newDirection;
    }

    void OnTriggerEnter2D(Collider2D col){
        if (col.tag == "Checkpoint"){
            SetDirection(col.GetComponent<Checkpoint>().Redirect());
        }
        if (col.tag == "Destroy"){
            StopCoroutine(HitCell());
            Destroy(gameObject);
        }
    }

    public bool IsHit(){
        return hit;
    }

    public IEnumerator HitCell(){
        hit = true;
        int repeat = 10;
        Color nextColor;
        while (repeat > 0){
            if (white){
                nextColor = ownSprite.color;
                nextColor.a = nextColor.a - 0.1f;
                ownSprite.color = nextColor;
                //Debug.Log(ownSprite.color);
                yield return new WaitForFixedUpdate();
                if (ownSprite.color.a <= 0){
                    Destroy(gameObject);
                    break;
                }
            } else {
                if (ownSprite.color.b == 0){
                    ownSprite.color = new Color(1,1,1,1);
                } else{
                    ownSprite.color = new Color(1,1,0,1);
                }
                yield return new WaitForSeconds(0.1f);
                repeat --;
            }
        }
    }
}