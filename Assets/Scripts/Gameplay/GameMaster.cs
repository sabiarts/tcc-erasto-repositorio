﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour
{
	int sickCellsDestroyed = 0;
    int whiteCellsDestroyed = 0;
    int wrongCells = 0;
	int energy;
	int score = 0;
	bool start = false;

	public List<Sprite> skins;

	public bool cheat = false;

	public int maxEnergy;
	public int energyCost;
	public Transform energyGauge;
	public float timescale_inicial;

	public Text whiteCellsText;
	public Transform whiteGauge;
	public Text sickCellsText;
	public Transform sickGauge;
	public Text wrongCellsText;
	public Transform wrongGauge;
	public Text energyText;

	public List<GameObject> allCells = new List<GameObject>();

	public GameObject gameOver;

	public GameObject analogCanvas;

	public GameObject tutorial;

	public GameObject reTutorial;

    void Start(){
    	Time.timeScale = timescale_inicial;
		energy = maxEnergy;
		WhichSkin();
		PlayerPrefs.SetInt("TurnSickCells", 0);
		UpdatePoints();
		if (SceneManager.GetActiveScene().name == "Level1"){
			if(PlayerPrefs.GetString("Tuto", "false") == "true"){
				reTutorial.SetActive(true);
			} else{
				tutorial.SetActive(true);
			}
		}
        //foreach (GameObject cell in GameObject.FindGameObjectsWithTag("Cell")){
		//	allCells.Add(cell);
		//}
    }

	void WhichSkin(){
		SpriteRenderer playerSkin = GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>();
		switch (PlayerPrefs.GetString("skin", "Skin Padrão")){
			case "Skin Padrão":
				playerSkin.sprite = skins[0];
				break;
			case "Skin Especial":
				playerSkin.sprite = skins[1];
				break;
			default:
				Debug.LogError("Defined player skin does not exist: " + PlayerPrefs.GetString("skin", "Skin Padrão"));
				break;
		}
	}

	void FixedUpdate(){
		if (start == false && allCells.Count>0){
			start = true;
			StartCoroutine(Pulse());
		}
	}

	IEnumerator Pulse(){
		GameObject toRemove = null;
		while (true){
			//Debug.Log("Heartbeat");
			if(allCells.Count > 0){
				foreach(GameObject cell in allCells){
					//Debug.Log(cell);
					if(cell == null){
						toRemove = cell;
					} else {
						//Debug.Log("Mover " + cell.ToString() + " na direção " + cell.transform.right);
						cell.GetComponent<Rigidbody2D>().AddForce((cell.GetComponent<Cell>().GetDirection() * Time.deltaTime) * 15000);
					}
				}
				allCells.Remove(toRemove);
			}
			yield return new WaitForSeconds(1f);
		}	
	}

	public void CellTouched(Cell cell){
		StartCoroutine(cell.HitCell());
		energy -= energyCost;
		energyGauge.position += Vector3.left * energyCost/maxEnergy * 5.03f;
		if (cell.white){
			if (cell.sick){
				sickCellsDestroyed++;
				sickGauge.position += Vector3.right * energyCost/maxEnergy * 3.13f;
				score += 3;
			} else {
				whiteCellsDestroyed++;
				whiteGauge.position += Vector3.right * energyCost/maxEnergy * 3.13f;
			}
		} else {
			wrongCells++;
			wrongGauge.position += Vector3.right * energyCost/maxEnergy * 3.13f;
		}
		UpdatePoints();
		if (energy < energyCost){
			energyGauge.position += Vector3.left * 10;
			EndLevel();
		}
	}

	void UpdatePoints(){
		whiteCellsText.text = whiteCellsDestroyed.ToString();
		sickCellsText.text = sickCellsDestroyed.ToString();
		wrongCellsText.text = wrongCells.ToString();
		energyText.text = "ENERGIA: " + energy.ToString();
	}

	public bool EnergyLeft(){
		if (energy >= energyCost){
			return true;
		} else {
			return false;
		}
	}

	void EndLevel(){
		analogCanvas.SetActive(false);
		GameObject removeCell;
		//StopCoroutine(Pulse());
		StopAllCoroutines();
		for(int cell = allCells.Count - 1; cell >=0 ; cell--){
			removeCell = allCells[cell];
			//StopCoroutine(removeCell.GetComponent<Cell>().HitCell());
			allCells.Remove(removeCell);
			Destroy(removeCell);
		}
		Destroy(GameObject.FindGameObjectWithTag("Player"));
		Destroy(GameObject.FindGameObjectWithTag("Respawn"));
		PlayerPrefs.SetInt("SickCells", PlayerPrefs.GetInt("SickCells", 0) + sickCellsDestroyed);
		PlayerPrefs.SetInt("TurnSickCells", sickCellsDestroyed);
		PlayerPrefs.SetInt("WhiteCells", PlayerPrefs.GetInt("WhiteCells", 0) + whiteCellsDestroyed);
		PlayerPrefs.SetInt("OtherCells", PlayerPrefs.GetInt("OtherCells", 0) + wrongCells);
		//PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("OtherCells", 0) + score);
		PlayerPrefs.SetInt("Gacha", PlayerPrefs.GetInt("Gacha", 0) + score);
		PlayerPrefs.SetString(SceneManager.GetActiveScene().name, "true");
		GetComponent<StickerManager>().CheckAllStickers();
		gameOver.SetActive(true);
		GetComponent<Gacha>().CheckGacha(score, cheat);
	}

	public void Pause(){
		Time.timeScale = 0;
	}

	public void Unpause(){
		Time.timeScale = 1;
	}

	public void SeenTutorial(){
		PlayerPrefs.SetString("Tuto", "true");
	}
}
//  