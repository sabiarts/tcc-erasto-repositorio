﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.AnimationModule;

public class Player : MonoBehaviour
{
    float moveSpeedX;
    float moveSpeedY;
    public float accel;
    public float maxSpeed;
    float direction;
    bool isMoving;

    //public List<Sprite> skins;

    Rigidbody2D body;
    GameMaster gameMaster;

    //public bool alive;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        gameMaster = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameMaster>();
        //GetComponent<SpriteRenderer>().sprite = gameMaster.skins[PlayerPrefs.GetInt("activeSkin", 0)];
        //alive = true;
    }

    void FixedUpdate(){
        PlayerMove();
        //if (GetComponent<Rigidbody2D>().velocity > maxSpeed){
        //GetComponent<Rigidbody2D>().velocity = Vector2.ClampMagnitude(GetComponent<Rigidbody2D>().velocity, maxSpeed);
        //Debug.Log(GetComponent<Rigidbody2D>().velocity.ToString());
    }


    void PlayerMove(){
        if (Mathf.Abs(body.velocity.x) <1){
            moveSpeedX = accel* 4 * Time.fixedDeltaTime * Input.GetAxisRaw("Horizontal");
        } else if (Mathf.Abs(body.velocity.x) > maxSpeed){
            moveSpeedX = accel/4 * Time.fixedDeltaTime * Input.GetAxisRaw("Horizontal");
        } else {
            moveSpeedX = accel * Time.fixedDeltaTime * Input.GetAxisRaw("Horizontal");
        }

        if (Mathf.Abs(body.velocity.y) <1){
            moveSpeedY = accel* 4 * Time.fixedDeltaTime * Input.GetAxisRaw("Vertical");
        } else if (Mathf.Abs(body.velocity.y) > maxSpeed){
            moveSpeedY = accel/4 * Time.fixedDeltaTime * Input.GetAxisRaw("Vertical");
        } else {
            moveSpeedY = accel * Time.fixedDeltaTime * Input.GetAxisRaw("Vertical");
        }
       body.AddForce(new Vector2(moveSpeedX, moveSpeedY));
        
        if (Input.GetAxisRaw("Horizontal") > 0){
            
            direction = 1;
            //Debug.Log("Speed in X: " + (body.velocity.x).ToString());
            //setIsMoving(true);

        }
        if (Input.GetAxisRaw("Horizontal") < 0){
            
            direction = -1;
            //Debug.Log("Speed in X: " + (body.velocity.x).ToString());
            //Debug.Log("<-");
            //setIsMoving(true);

        }
        if (Input.GetAxisRaw("Vertical") != 0){
            //Debug.Log("Speed in Y: " + (body.velocity.y).ToString());
            //setIsMoving(true);
        }
        if (Input.GetAxisRaw("Horizontal"
        ) == 0 && Input.GetAxisRaw("Vertical") == 0){
            //Debug.Log("x");
            //setIsMoving(false);
        }
        SpriteWork();
    }

    public bool setIsMoving(bool mov){
        isMoving = mov;
        //Debug.Log(mov);
        //gameObject.GetComponent<Animator>().SetBool("isMoving", mov);

        return isMoving;
    }

    void SpriteWork(){
        if (direction == 1){
            if (gameObject.GetComponent<Transform>().localScale.x < 0){
                gameObject.GetComponent<Transform>().localScale *= new Vector2(-1,1);
                gameObject.GetComponent<Transform>().localScale = new Vector3(gameObject.GetComponent<Transform>().localScale.x,gameObject.GetComponent<Transform>().localScale.y,1);
            }
        }
        if (direction == -1){
            if (gameObject.GetComponent<Transform>().localScale.x > 0){
                gameObject.GetComponent<Transform>().localScale *= new Vector2(-1,1);
                gameObject.GetComponent<Transform>().localScale = new Vector3(gameObject.GetComponent<Transform>().localScale.x,gameObject.GetComponent<Transform>().localScale.y,1);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D col){
        if (col.gameObject.tag == "Cell"){
            GetComponent<AudioSource>().Play();
            //Debug.Log("BATEU!!!");
            Cell cell = col.gameObject.GetComponent<Cell>();
            if (!cell.IsHit() && gameMaster.EnergyLeft()){
                gameMaster.CellTouched(cell);
            }
        }
    }

}