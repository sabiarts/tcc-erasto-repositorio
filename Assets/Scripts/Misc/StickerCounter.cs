﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StickerCounter : MonoBehaviour
{
    string[] stickerNames;
    int stickerCounter = 0;
    Text totalStikers;

    // Start is called before the first frame update
    void Start()
    {
        totalStikers = GetComponent<Text>();
        stickerNames = new string[5] {"st_tratamento1", "st_tratamento2", "st_tratamento3", "st_tratamento4", "st_tratamento5"};
        CheckAllStickers();
    }

    void CheckAllStickers(){
        foreach (string sticker in stickerNames){
            if (PlayerPrefs.GetString(sticker, "false") == "true"){
                stickerCounter++;
                Debug.Log(sticker + " adiquirido");
            }
            totalStikers.text = stickerCounter.ToString() + "/18";
        }
    }
}
