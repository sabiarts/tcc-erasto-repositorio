﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public bool clearData;
    public Scene nextLevel;

    void Start(){
        if (clearData){
            PlayerPrefs.DeleteAll();
        }
    }
    
    public void ChangeScene(string sceneName){
        SceneManager.LoadScene(sceneName);
    }

    public void Retry(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void NextLevel(){
        ChangeScene(nextLevel.name);
    }
}
