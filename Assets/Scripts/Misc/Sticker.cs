﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sticker : MonoBehaviour
{
    public string stickerID;
    public bool taken = false;

    Image stickerImage;
    Button stickerButton;

    //bool ready = false;
    
    void Start()
    {
        stickerImage = GetComponent<Image>();
        stickerButton = GetComponent<Button>();
        Debug.Log(stickerID + ": " + PlayerPrefs.GetString(stickerID, "false"));
        if (PlayerPrefs.GetString(stickerID, "false") == "true"){
            stickerImage.color = new Color(1,1,1,1);
            stickerButton.interactable = true;
            taken = true;
        } else {
            stickerImage.color = new Color(1,1,1,0);
            stickerButton.interactable = false;
        }
        //ready = true;
    }
}
