﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StickerManager : MonoBehaviour
{

    /*======= NOME DOS STICKERS DO TRATAMENTO EM ORDEM =========
    st_tratameto1 - descobrimento precoce
    st_tratameto2 - cançaço e sonolencia
    st_tratameto3 - sintomas são diferentes
    st_tratameto4 - o que fazer (raro)
    st_tratameto5 - sentimentos (lendário) 
    */

    bool newSticker = false;
    //int stickerCounter;
    //public Text totalStickers;

    public void CheckAllStickers(){
        //stickerCounter = 0;
        CheckSticker("st_tratamento1", "common");
        CheckSticker("st_tratamento2", "common");
        CheckSticker("st_tratamento3", "common");
        CheckSticker("st_tratamento4", "rare");
        CheckSticker("st_tratamento5", "legendary");
    }

    void CheckSticker(string stickerID, string type){
        bool conditionMet = false;
        if(PlayerPrefs.GetString(stickerID, "false") == "true"){
            return;
        }

        switch(stickerID){
            case "st_tratamento1":
                //condição do sticker
                if (PlayerPrefs.GetInt("Gacha", 0) > 0){
                    conditionMet = true;
                }
                break;
            case "st_tratamento2":
                if (PlayerPrefs.GetInt("TurnSickCells", 0) > 25){
                    conditionMet = true;
                }
                break;
            case "st_tratamento3":
                 if (PlayerPrefs.GetInt("SickCells", 0) > 100){
                    conditionMet = true;
                }
                break;
            case "st_tratamento4":
                 if (PlayerPrefs.GetInt("SickCells", 0) > 250){
                    conditionMet = true;
                }
                break;
            case "st_tratamento5":
                 if (PlayerPrefs.GetString("Level1", "false") == "true" &&
                 PlayerPrefs.GetString("Level2", "false") == "true" &&
                 PlayerPrefs.GetString("Level3", "false") == "true" &&
                 PlayerPrefs.GetString("Level4", "false") == "true" &&
                 PlayerPrefs.GetString("Level5", "false") == "true"){
                    conditionMet = true;
                }
                break;
        }
        if (conditionMet){
            newSticker = true;
            PlayerPrefs.SetString(stickerID, "true");
            PlayerPrefs.SetInt(type, PlayerPrefs.GetInt(type, 0) + 1);
            //stickerCounter++;
        }
    }

}
