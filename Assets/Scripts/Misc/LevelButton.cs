﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    public int levelNumber;
    void Start()
    {
        if(levelNumber > 1){
            if(PlayerPrefs.GetString("Level" + (levelNumber - 1).ToString(), "false") == "true"){
                GetComponent<Button>().interactable = true;
            }
        }   
    }

}
