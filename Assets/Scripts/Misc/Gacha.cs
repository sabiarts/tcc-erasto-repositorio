﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gacha : MonoBehaviour
{
    AudioSource soundEffects;

    public int delay = 1;

    public int increaseRate = 30;

    public Text scoreText;
    public Text gachaScoreText;
    public Text gachaEarnedText;
    public Transform gachaGauge;
    public Transform fullGauge;

    public GameObject continueButton;
    public GameObject retryButton;
    public GameObject nextButton;
    public GameObject prizeButton;
    public GameObject PrizeScreen;
    public Text prizeGachaText;

    public Transform chest;
    public Transform prize1;
    public Transform prize2;

    public Text prize1Text;
    public Text prize2Text;

    public List<Sprite> allRewards;

    void Start(){
        soundEffects = GetComponent<AudioSource>();
    }

    public void CheckGacha(int score, bool cheat)
    {
        soundEffects.Play();
        int currentGacha = PlayerPrefs.GetInt("Gacha", 0);
        int gachaCost = PlayerPrefs.GetInt("GachaCost", 120);
        //int gachaCost = 20; //<=============== DESCOMENTAR PARA TESTES!!!!!!!
        bool wonPrize = false;
        UpdateGachaGauge(currentGacha, gachaCost);
        scoreText.text = "X3= " + score.ToString() + " PONTOS!!!";
        gachaEarnedText.text = "+" + score.ToString();
        UpdateScoreText(currentGacha, gachaCost);
        currentGacha += score;
        if (currentGacha >= gachaCost || cheat){
            wonPrize = true;
            PlayerPrefs.SetInt("Gacha", currentGacha - gachaCost);
            if (currentGacha < 500){
                PlayerPrefs.SetInt("GachaCost", gachaCost + increaseRate);
            }
        }
        StartCoroutine(ScreenGachaResult(currentGacha, gachaCost, wonPrize));
    }

    IEnumerator ScreenGachaResult(int current, int limit, bool prize){
        if (current > limit){
            current = limit;
        }
        yield return new WaitForSeconds(delay);
        scoreText.gameObject.SetActive(true);
        soundEffects.Play();
        yield return new WaitForSeconds(delay);
        gachaEarnedText.gameObject.SetActive(true);
        soundEffects.Play();
        yield return new WaitForSeconds(delay);
        UpdateScoreText(current, limit);
        UpdateGachaGauge(current, limit);
        soundEffects.Play();
        yield return new WaitForSeconds(delay);
        if (prize){
            prizeButton.SetActive(true);
        }else {
            continueButton.SetActive(true);
            retryButton.SetActive(true);
            if (nextButton != null){
                nextButton.SetActive(true);
            }
        }
    }


    public void GetPrize(){
            Rewards();
            StartCoroutine(Prize());
    }

    
    IEnumerator Prize(){
        PrizeScreen.SetActive(true);
        prizeGachaText.text = gachaScoreText.text;
        yield return new WaitForSeconds(delay);
        while (chest.localScale.x > 0.1f){
            ReScale(chest, new Vector2(0.1f, 0.1f));
            yield return new WaitForFixedUpdate();
        }
        while (prize1.localScale.x < 1){
            ReScale(prize1, new Vector2(1, 1));
            ReScale(prize2, new Vector2(1, 1));
            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForSeconds(delay);
        prize1Text.gameObject.SetActive(true);
        prize2Text.gameObject.SetActive(true);
    }

    void Rewards(){
        string reward1;
        int reward2 = (int) Random.Range(1f, 5f) * 10;
        if (PlayerPrefs.GetString("Skin Especial", "false") == "false" && (Random.Range(1f, 100f) >= 80)){
            reward1 = "Skin Especial"; 
        } else {
            reward1 = "Cash";
        }
        switch (reward1){
            case "SpecialSkin":
                Debug.Log("Vai dar skin!");
                prize1Text.text = "Nova Skin!";
                PlayerPrefs.SetString("Skin Especial", "true");
                prize1.gameObject.GetComponent<Image>().sprite = allRewards[1];
                break;
            
            case "Cash":
                Debug.Log("Vai dar grana!");
                prize1Text.text = "50 moedas!";
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins", 0) + 50);
                prize1.gameObject.GetComponent<Image>().sprite = allRewards[0];
                break;
        }

        prize2Text.text = reward2.ToString() + " moedas!";
        PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins", 0) + reward2);

        
    }

    void ReScale(Transform objTransform, Vector2 tgtScale){
        objTransform.localScale = Vector2.Lerp(objTransform.localScale, tgtScale, 8f * Time.deltaTime);
            if (Vector3.Distance(objTransform.localScale, tgtScale) < 0.01f){
				objTransform.localScale = tgtScale;
            }
    }

    void UpdateGachaGauge(int current, int limit){
        if (current >= limit){
            current = limit;
            gachaGauge.position = fullGauge.position; //current/limit
        } else {
            gachaGauge.position -= Vector3.left * current/limit * 2.5f; //current/limit
        }
    }

    void UpdateScoreText(int current, int limit){
        gachaScoreText.text = current.ToString() + "/" + limit.ToString() + " PONTOS!!!";
    }

    
}
