﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public Text coinsText;

    int coins; 
    bool activeShop = false;

    //public Text totalStickers;

    void Start(){
        if (PlayerPrefs.GetString("Skin Padrão", "true") != "true"){
            PlayerPrefs.SetString("Skin Padrão", "true");
        }
        //CountStickers();
    }
    
    void FixedUpdate()
    {
        if (activeShop){
            coins = PlayerPrefs.GetInt("Coins", 0);
            coinsText.text = "Você tem " + coins.ToString() + " moedas!";
            //coinsText.text = "DGSUFSJFASKJBHAESKJHBAEFKJBFASHBKJAFS";
        }
    }

    public void ActivateShop(bool onOff){
        activeShop = onOff;
    }

    public int GetCoins(){
        return coins;
    }

    public void CoinTransaction(int transaction){
        //Transação de moedas do ponto de vista do jogador. Pode ser positiva ou negativa.
        coins += transaction;
        coinsText.text = coins.ToString();
    }

    /* -----------------------------------

    void CountStickers(){
        GameObject[] stickers = GameObject.FindGameObjectsWithTag("Sticker");
        int stickerCounter = 0;
        while(stickerCounter < stickers.Length){
            if (stickers[stickerCounter].GetComponent<Sticker>().IsReady()){
                stickerCounter++;
            }
        }
        stickerCounter = 0;
        foreach (GameObject sticker in stickers){
            if (sticker.GetComponent<Sticker>().taken){
                Debug.Log("taken");
                stickerCounter++;
            }
        }
        Debug.Log("Stickers adiquiridos: " + stickerCounter.ToString());
        totalStickers.text = stickerCounter.ToString() + "/18";
    }*/
}
