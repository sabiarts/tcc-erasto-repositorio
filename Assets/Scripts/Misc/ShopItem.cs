﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    public string itemName;
    public string itemLabel;

    public int price;

    bool sold = false;

    ShopManager shopManager;

    public Button useButton;
    public Button buyButton;

    public Text nameText;
    public Text priceText;

    Image itemImage;
    

    void Start()
    {
        itemImage = GetComponent<Image>();
        shopManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<ShopManager>();
        nameText.text = itemLabel;
        priceText.text = price.ToString();
        if (itemName == "Skin Padrão"){
            PlayerPrefs.SetString(itemName, "true");
        }
        if (PlayerPrefs.GetString(itemName, "false") == "true"){
            //Debug.Log(itemName + " deu ruim!");
            sold = true;
            buyButton.interactable = false;
            buyButton.GetComponentInChildren<Text>().text = "Comprado";
            if(useButton != null){
                useButton.interactable = true;
            }
        } else {
            buyButton.interactable = true;
            useButton.interactable = false;
        }
    }

    void FixedUpdate(){
        if (sold || shopManager.GetCoins() < price){
            buyButton.interactable = false;
        } else{
            buyButton.interactable = true;
        }
        if (useButton != null) {
            if (PlayerPrefs.GetString("skin", "Skin Padrão") == itemName){
                useButton.interactable = false;
                useButton.GetComponentInChildren<Text>().text = "Usando";
                itemImage.color = new Color(0.4764151f,0.6651033f,1,1);
            } else{
                if (sold){
                    useButton.interactable = true;
                } else {
                    useButton.interactable = false;
                }
                itemImage.color = new Color(1,1,1,1);
                useButton.GetComponentInChildren<Text>().text = "Usar";
            }
             
        }
    }

    public void Buy(){
        if (shopManager.GetCoins() >= price){
            shopManager.CoinTransaction(price*-1);
            PlayerPrefs.SetString(itemName, "true");
            buyButton.interactable = false;
            buyButton.GetComponentInChildren<Text>().text = "Comprado";
            if(useButton != null){
                useButton.interactable = true;
            }
        }
    }

    public void Use(){
        if (PlayerPrefs.GetString(itemName, "false") == "true"){
            PlayerPrefs.SetString("skin", itemName);
        }
        Debug.Log(PlayerPrefs.GetString("skin"));
    }
}
